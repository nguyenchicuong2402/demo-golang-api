package main

import (
	"demo-api/auth"
	"demo-api/person"
	"demo-api/utils"
	"github.com/gorilla/mux"
	_ "github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"net/http"
	"os"
)

func loadEnv() {
	env := os.Getenv("ENV_MODE")

	if len(env) > 0 && env == "prod" {
		utils.LoadEnv("prod.env")
	} else {
		utils.LoadEnv("dev.env")
	}
}

func main() {
	loadEnv()

	router := mux.NewRouter()

	// APIs Auth
	router.HandleFunc("/login", auth.Auth).Methods("POST")

	// Restful APIs Person
	router.HandleFunc("/", auth.AuthMiddleware(person.GetListPerson)).Methods("GET")
	router.HandleFunc("/", auth.AuthMiddleware(person.PostPerson)).Methods("POST")

	if err := http.ListenAndServe(":5000", router); err != nil {
		panic(err)
	}
}

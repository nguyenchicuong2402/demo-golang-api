FROM golang:1.14-alpine

WORKDIR /go/src/app

ENV ENV_MODE prod

COPY . .

RUN go get -d -v ./...

RUN go install -v ./...

RUN go build -o ./api

CMD ["./api"]
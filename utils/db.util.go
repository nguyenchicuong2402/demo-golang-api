package utils

import (
	"database/sql"
	"fmt"
)

func ConnectDB() *sql.DB {
	connStr := GetEnv("DB_URI")
	db, err := sql.Open("postgres", connStr)

	if err != nil {
		fmt.Println(err)
	}

	return db
}
package utils

import (
	"encoding/json"
	"net/http"
)

type ErrorResponse struct {
	Message 	string	`json:"message"`
	StatusCode	int		`json:"status_code"`
}

func ResponseWithJSON(response http.ResponseWriter, statusCode int, data interface{}) {
	result, _ := json.Marshal(data)
	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(statusCode)
	_, _ = response.Write(result)
}

func ResponseError(res http.ResponseWriter, statusCode int, message string) {
	var errorResponse = ErrorResponse{
		Message: message,
		StatusCode: statusCode,
	}
	ResponseWithJSON(res, statusCode, errorResponse)
}
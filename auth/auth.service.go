package auth

import (
	"demo-api/utils"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"regexp"
	"strings"
	"time"
	_ "time"
)

func GenerateJWT(username string) string {
	claims := AuthClaim{
		username,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			Issuer:    username,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secretKey := []byte(utils.GetEnv("SECRET_KEY"))
	tokenString, err := token.SignedString(secretKey)

	if err != nil {
		return err.Error()
	}

	return tokenString
}

func ValidateJWT(tokenString string) (bool, string) {
	if matched, err := regexp.MatchString(`^Bearer\s\w.+$`, tokenString); err != nil && !matched {
		return false, ""
	}

	tokenString = strings.Split(tokenString, "Bearer ")[1]
	token, _ := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		secretKey := []byte(utils.GetEnv("SECRET_KEY"))
		return secretKey, nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)

	username := claims["username"].(string)

	return ok && token.Valid, username
}

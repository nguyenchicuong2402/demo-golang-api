package auth

type AuthDTO struct {
	Token 	string	`json:"token"`
	Type 	string	`json:"type"`
}

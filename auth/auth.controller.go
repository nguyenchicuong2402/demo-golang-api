package auth

import (
	"demo-api/utils"
	"encoding/json"
	"net/http"
)

const USERNAME = "admin"
const PASSWORD = "admin"

func Auth(res http.ResponseWriter, req *http.Request) {
	var authPayload AuthPayload
	err := json.NewDecoder(req.Body).Decode(&authPayload)

	if err != nil {
		http.Error(res, err.Error(), http.StatusBadRequest)
	}

	if authPayload.Username == USERNAME && authPayload.Password == PASSWORD {
		var token = GenerateJWT(authPayload.Username)
		var authDTO = AuthDTO{Token: token, Type: "Bearer"}
		utils.ResponseWithJSON(res, http.StatusOK, authDTO)
	} else {
		utils.ResponseWithJSON(res, http.StatusForbidden, "Tài khoản không chính xác")
	}
}

func AuthMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		var headers = req.Header
		var authorization = headers.Get("Authorization")

		if len(authorization) == 0 {
			utils.ResponseError(res, http.StatusUnauthorized, "Vui lòng đăng nhập để tiếp tục")
		} else if ok, username := ValidateJWT(authorization); ok && username == "admin" {
			next.ServeHTTP(res, req)
		} else {
			utils.ResponseError(res, http.StatusUnauthorized, "Bạn không có quyền thực hiện hoặc token đã hết hạn")
		}
	}
}

package auth

import "github.com/dgrijalva/jwt-go"

type AuthClaim struct {
	Username	string	`json:"username"`
	jwt.StandardClaims
}

package person

import (
	"demo-api/utils"
)

func CreatePerson(person Person) Person {
	var db = utils.ConnectDB()
	var id int

	var sql = "INSERT INTO PERSON(NAME) VALUES($1) RETURNING id"
	_ = db.QueryRow(sql, person.Name).Scan(&id)
	person.Id = id

	return person
}

func FindAll() []Person {
	var db = utils.ConnectDB()
	var persons []Person

	var sql = "SELECT * FROM PERSON"
	rows, err := db.Query(sql)

	if err != nil {
		panic(err.Error())
	} else {
		for rows.Next() {
			var (
				id int
				name string
			)
			err := rows.Scan(&name, &id)

			if err != nil {
				panic(err.Error())
			} else {
				person := Person{
					Id: id,
					Name: name,
				}
				persons = append(persons, person)
			}
		}
	}

	return persons
}

package person

import (
	"demo-api/utils"
	_ "demo-api/utils"
	"encoding/json"
	"net/http"
)

func PostPerson(res http.ResponseWriter, req *http.Request) {
	var person Person

	err := json.NewDecoder(req.Body).Decode(&person)
	if err != nil {
		http.Error(res, err.Error(), http.StatusBadRequest)
		return
	}

	utils.ResponseWithJSON(res, http.StatusOK, CreatePerson(person))
}

func GetListPerson(res http.ResponseWriter, req *http.Request) {
	listPerson := FindAll()
	utils.ResponseWithJSON(res, http.StatusOK, listPerson)
}